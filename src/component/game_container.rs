use yew::prelude::*;

pub struct GameContainer {
    data: [[usize; 4]; 4],
}

impl Component for GameContainer {
    type Message = ();
    type Properties = ();
    fn create(_: Self::Properties, _: ComponentLink<Self>) -> Self {
        Self {

        }
    }

    fn update(&mut self, _: Self::Message) -> ShouldRender {
        true // 指示组件应该重新渲染
    }

    fn change(&mut self, _props: Self::Properties) -> ShouldRender {
        false
    }

    fn view(&self) -> Html {
        html! {
            <div class="game-container">
                <div class="game-message">
                    <div class="lower">
                        <a class="retry-button">{"Try again"}</a>
                    </div>
                </div>
            </div>
        }
    }
}
