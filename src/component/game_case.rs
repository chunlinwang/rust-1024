use std::io;
use yew::prelude::*;
use crate::game::Matrix;

pub struct GameCase {
    props: Props,
}

#[derive(Clone, Properties)]
pub struct Props {
    pub value: u64,
}

pub enum Msg {
    AddOne,
}

impl Component for GameCase {
    type Message = Msg;
    type Properties = Props;
    fn create(props: Self::Properties, _: ComponentLink<Self>) -> Self {
        GameCase {
            props,
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        true // 指示组件应该重新渲染
    }

    fn change(&mut self, _props: Self::Properties) -> ShouldRender {
        false
    }

    fn view(&self) -> Html {
        html! {
          <div class="grid-cell">{self.props}</div>
        }
    }
}
