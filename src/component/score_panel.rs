use yew::prelude::*;

pub struct ScorePanel {
    props: ScoreProps,
}

#[derive(Clone, Properties)]
pub struct ScoreProps {
    pub score: u64,
    pub best: u64,
}

impl Component for ScorePanel {
    type Message = ();
    type Properties = ScoreProps;

    fn create(props: Self::Properties, _: ComponentLink<Self>) -> Self {
        ScorePanel { props }
    }

    fn update(&mut self, _: Self::Message) -> ShouldRender {
        true // 指示组件应该重新渲染
    }

    fn change(&mut self, _props: Self::Properties) -> ShouldRender {
        false
    }

    fn view(&self) -> Html {
        html! {
            <div class="scores-container">
                <div class="score-container">{self.props.score}</div>
                <div class="best-container">{self.props.best}</div>
            </div>
        }
    }
}
