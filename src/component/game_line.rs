use std::io;
use yew::prelude::*;
use crate::game::Matrix;
use crate::component::game_case::GameCase;

pub struct GameLine {
    props: Props,
}

#[derive(Clone, Properties)]
pub struct Props {
    pub value: [u64; 4],
}

pub enum Msg {
    AddOne,
}

impl Component for GameLine {
    type Message = Msg;
    type Properties = Props;
    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        Self {
            props,
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        true // 指示组件应该重新渲染
    }

    fn change(&mut self, _props: Self::Properties) -> ShouldRender {
        false
    }

    fn view(&self) -> Html {
        html! {
            <div class="grid-row">
            {for self.props.value.iter().map(|mut item| {
                <GameCase props={item} />
            })}
        }
    }
}
