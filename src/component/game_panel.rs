use std::io;
use yew::prelude::*;
use crate::game::Matrix;
use crate::component::score_panel::ScorePanel;
use crate::component::game_container::GameContainer;


pub struct GamePanel {
    pub matrix: Matrix,
}

impl Component for GamePanel {
    type Message = ();
    type Properties = ();
    fn create(_: Self::Properties, _: ComponentLink<Self>) -> Self {
        Self {
            matrix: Matrix::new(),
        }
    }

    fn update(&mut self, _: Self::Message) -> ShouldRender {
        true // 指示组件应该重新渲染
    }

    fn change(&mut self, _props: Self::Properties) -> ShouldRender {
        false
    }

    fn view(&self) -> Html {
        html! {
            <div class="container">
            <div class="heading">
              <h1 class="title">{"1024"}</h1>
              <ScorePanel score={self.matrix.score}, best=0 />
            </div>
                <p class="game-intro">{"Join the numbers and get to the"} <strong>{"1024 tile!"}</strong></p>
                <GameContainer data={self.matrix.data}/>
            </div>
        }
    }
}
